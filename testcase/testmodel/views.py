from django.shortcuts import render

# Create your views here.
import datetime
from django.http import HttpResponse, HttpResponseNotFound


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)